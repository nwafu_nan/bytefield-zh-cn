# bytefield-zh-cn
bytefield协议数据包和内存结构绘图排版宏包使用手册翻译，欢迎大家批评指正。
#### 介绍
[bytefield协议数据包和内存结构绘图排版宏包使用手册](https://www.ctan.org/pkg/bytefield)说明书的中文翻译，以方便后期查阅和使用。

#### 说明
1. 为保持与原说明手册格式一致，仅在原`bytefield.dtx`文档类中添加了`ctex`宏包的调用。
2. 仅在TeXLive2021+Ubuntu 20.04下用`xelatex bytefield.dtx`进行了编译，不保证其它发行版和操作平台的编译正确性。

### 贡献
由于受能力所限，无保证翻译的准确性和合理性。如果您有任何改进意见或者功能需求，欢迎提交 [issue](https://gitee.com/nwafu_nan/bytefield-zh-cn/issues) 或 [pull request](https://gitee.com/nwafu_nan/bytefield-zh-cn/pulls)。

